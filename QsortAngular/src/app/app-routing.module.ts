import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/seller-dashboard/home/home.component';
import { AuthguardService } from './auth-guard';
import { SiteLayoutComponent } from './components/_layout/site-layout/site-layout.component';
import { OrdersComponent } from './components/seller-dashboard/orders/orders.component';
import { InventoryComponent } from './components/seller-dashboard/inventory/inventory.component';
import { PaymentDataComponent } from './components/seller-dashboard/payment-data/payment-data.component';
import { ProductProfitabilityComponent } from './components/seller-dashboard/product-profitability/product-profitability.component';
import { OrderAnalysisComponent } from './components/seller-dashboard/order-analysis/order-analysis.component';
// import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MyAccountComponent } from './components/my-account/my-account.component';

const routes: Routes = [


  { path: '', component: LoginComponent},

    // Site routes goes here
    {
      path: '',
      component: SiteLayoutComponent,
      children: [

        { path: 'seller-home', component: HomeComponent,canActivate: [AuthguardService]},
        // { path: 'orders', component: OrdersComponent,canActivate: [AuthguardService]},
       { path : 'inventory', component:InventoryComponent, canActivate:[AuthguardService]},
      //  { path : 'payment', component:PaymentDataComponent, canActivate:[AuthguardService]},
       { path : 'product-profitability', component:ProductProfitabilityComponent, canActivate:[AuthguardService]},
      { path : 'order-analysis', component:OrderAnalysisComponent, canActivate:[AuthguardService]},
      { path : 'my-account', component:MyAccountComponent, canActivate:[AuthguardService]},



      ]
  },


 //no layout routes

 { path: 'login', component: LoginComponent,canActivate: [AuthguardService]},

 // otherwise redirect to home
 { path: '**', redirectTo: '' }






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
