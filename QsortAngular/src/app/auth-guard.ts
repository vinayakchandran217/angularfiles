import { Injectable} from '@angular/core';
import { CanActivate , Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';

@Injectable()
export class AuthguardService implements CanActivate{





  // isLoggedIn: boolean;

  constructor(
    private auth: AuthService,
    private myRoute: Router

  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.auth.isLoggedIn() == true){
      console.log("loggedin");
      return true;
    }
    if(this.auth.isLoggedIn() == false){
      this.myRoute.navigate(["login"]);
      console.log("not loggedin");
      return false;
    }
  }

}



