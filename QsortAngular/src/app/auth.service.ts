import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()

export class AuthService {
  loggedinStatus = false;

  constructor(private myRoute: Router) { }

  setLoggedin(value: boolean){



    this.loggedinStatus = value;

  }
   isLoggedIn() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var status = currentUser.status;

   if(status =='1'){
    // console.log(status);
    return this.loggedinStatus =true;
}
else{
  // console.log(status);
  return this.loggedinStatus =false;
}
  }



  logout() {
     this.loggedinStatus =false;
    localStorage.clear();

    this.myRoute.navigate(["Login"]);
  }

}
