import { TestBed } from '@angular/core/testing';

import { QsortService } from './qsort.service';

describe('QsortService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QsortService = TestBed.get(QsortService);
    expect(service).toBeTruthy();
  });
});
