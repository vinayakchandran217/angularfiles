import { Injectable } from '@angular/core';
import {Headers , RequestOptions } from '@angular/http';
import { Login } from '../Models/login';
import { Inventory } from '../Models/Inventory';
import {Orders} from '../Models/Orders';
import {CostPrice} from '../Models/CostPrice';
import {Seller} from '../Models/seller';
import { HttpClient,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';  //load without refreshing
@Injectable({
  providedIn: 'root'
})
export class QsortService {

 server:string= "http://getqsort.com/lvqsfiles/";

  //server:string= "http://localhost/qsort-app/qsort/";
headers :Headers = new Headers();
options:any;

  constructor(private http:HttpClient) {
this.headers.append('enctype','multipart/form-data');
this.headers.append('Content-Type','application/json');
this.headers.append('X-Requested-With','XMLHttpRequest');
this.headers.append('Access-Control-Allow-Origin','*');
this.headers.append("Access-Control-Allow-Headers", "content-type, withcredentials, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
this.headers.append("Access-Control-Allow-Credentials", "true");
this.headers.append("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS, POST, PUT");
this.options = new RequestOptions({headers:this.headers});


  }
  login(email,password):Observable<Login>
  {

const newlogin = new Login(email,password);

return this.http.post<Login>(this.server+'login_c',newlogin);
  }


  get_orders_by_date(from,to,user):Observable<Orders>
{

const dates = new Orders(from,to,user);

return this.http.post<Orders>(this.server+'post_orders_by_date_range',dates);
}



GetTodayOrders(){
  return this.http.get(this.server+'ListOrders');
}


GetInventory():Observable<Inventory[]>{
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));


let userid = new HttpParams().set('userid',currentUser.user);

  return this.http.get<Inventory[]>(this.server+'req_seller_inventory',{params:userid});
}
get_seller_inventory():Observable<Inventory[]>{

  var currentUser = JSON.parse(localStorage.getItem('currentUser'));


  let userid = new HttpParams().set('userid',currentUser.user);

    return this.http.get<Inventory[]>(this.server+'get_seller_inventory',{params:userid});

}
get_payment_by_date(from,to,user):Observable<Orders>
{

const dates = new Orders(from,to,user);

return this.http.post<Orders>(this.server+'post_payment_by_date_range',dates);
}

get_product_analysis(from,to,user):Observable<Orders>
{

const dates = new Orders(from,to,user);

return this.http.post<Orders>(this.server+'post_product_analysis',dates);
}

get_product_profitability(from,to,user):Observable<Orders>
{

  const dates = new Orders(from,to,user);

return this.http.post<Orders>(this.server+'post_product_profitability',dates);
}


get_chart_sales():Observable<Seller[]>{

  var currentUser = JSON.parse(localStorage.getItem('currentUser'));


  let userid = new HttpParams().set('userid',currentUser.user);

    return this.http.get<Seller[]>(this.server+'post_selling_amount_for_chart',{params:userid});

}


update_cost(sku,price,user):Observable<CostPrice>
{

  const dates = new CostPrice(sku,price,user);

return this.http.post<CostPrice>(this.server+'update_cost',dates);
}


}
