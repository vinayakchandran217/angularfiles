import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'site-nav',
  templateUrl: './site-nav.component.html',
  styleUrls: ['./site-nav.component.css']
})
export class SiteNavComponent implements OnInit {

  constructor(private Auth:AuthService,private router: Router) { }

  ngOnInit() {
  }

  logoutClicked(){

this.Auth.logout();

  }
  myAccountClicked(){
  this.router.navigate(['/my-account']);
  }
}
