import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteNav2Component } from './site-nav2.component';

describe('SiteNav2Component', () => {
  let component: SiteNav2Component;
  let fixture: ComponentFixture<SiteNav2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteNav2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteNav2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
