import { Component, OnInit } from '@angular/core';
import {Orders} from '../../../Models/Orders';
import { QsortService } from '../../../service/qsort.service';
@Component({
  selector: 'app-payment-data',
  templateUrl: './payment-data.component.html',
  styleUrls: ['./payment-data.component.css']
})
export class PaymentDataComponent implements OnInit {

  from:string;
  to:string;
  payment: Orders[];
  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  user = this.currentUser.user;
  dtOptions: DataTables.Settings = {scrollX: true,
    scrollY: '400',responsive: true,
     };






  constructor(private service: QsortService) { }

  ngOnInit() {
  }

  payment_by_date(e){
    e.preventDefault();

    this.service.get_payment_by_date(this.from, this.to,this.user).subscribe((data)=>{

      var info=JSON.parse(JSON.stringify(data))
      this.payment=info.data;

// console.log(data);
    });

  }
}
