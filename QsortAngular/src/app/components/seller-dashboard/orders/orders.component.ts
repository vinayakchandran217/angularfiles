import {Component, OnInit} from '@angular/core';
import {Orders} from '../../../Models/Orders';
import { QsortService } from '../../../service/qsort.service';

@Component({
  selector: 'orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {


  from:string;
  to:string;



  dtOptions: DataTables.Settings = {scrollX: true,
    scrollY: '400',responsive: true,
     };




orders: Orders[];
cancelled_orders: Orders[];
pending_orders: Orders[];
refunds: Orders[];
currentUser = JSON.parse(localStorage.getItem('currentUser'));

  user = this.currentUser.user;

  constructor(private service: QsortService) { }

  ngOnInit() {

  }

  order_by_date(e){
    e.preventDefault();

    this.service.get_orders_by_date(this.from, this.to,this.user).subscribe((data)=>{

      var info=JSON.parse(JSON.stringify(data))
      this.orders=info.orders;
      this.cancelled_orders=info.cancelled_orders;
      this.pending_orders=info.pending_orders;
      this.refunds=info.refunds;
      // const table: any = $('table');
      // this.dataTable = table.DataTable();
console.log(info.refunds);
    });

  }

}
