import { Component, OnInit } from '@angular/core';
import {Orders} from '../../../Models/Orders';
import { QsortService } from '../../../service/qsort.service';
import * as moment from 'moment';
import { Daterange } from 'src/app/Models/daterange';
@Component({
  selector: 'app-order-analysis',
  templateUrl: './order-analysis.component.html',
  styleUrls: ['./order-analysis.component.css']
})
export class OrderAnalysisComponent implements OnInit {
  dtOptions: any = {};

  from:string;
  to:string;
  daterange:Daterange;
  // dtOptions: DataTables.Settings = {scrollX: true,
  //   scrollY: '400',responsive: true,
  //    };


data:Orders[];
  orders: Orders[];
  shipped_orders:Orders[];
  payment: Orders[];
  returns : Orders[];
  total_orders : Orders[];
  cancelled_orders : Orders[];
  return_percent : Orders[];
  order_sum :Orders[];
  ret_sum : Orders[];
  expected_rec_amt : Orders[];



  selected: any;

alwaysShowCalendars: boolean;
showRangeLabelOnInput: boolean;
keepCalendarOpeningWithRange: boolean;
maxDate: moment.Moment;
minDate: moment.Moment;
invalidDates: moment.Moment[] = [];
ranges: any = {
  // Today: [moment(), moment()],
  // Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  'This Month': [moment().startOf('month'), moment().endOf('month')],
  'Last Month': [
    moment()
      .subtract(1, 'month')
      .startOf('month'),
    moment()
      .subtract(1, 'month')
      .endOf('month')
  ]
};
isInvalidDate = (m: moment.Moment) =>  {
  return this.invalidDates.some(d => d.isSame(m, 'day') )
}



  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  user = this.currentUser.user;
  constructor(private service: QsortService) {


    this.maxDate = moment().add(2,  'weeks');
    this.minDate = moment().subtract(3, 'days');
    this.alwaysShowCalendars = true;
    this.keepCalendarOpeningWithRange = true;
    this.showRangeLabelOnInput = true;
    this.selected = {startDate: moment().subtract(1, 'days'), endDate: moment().subtract(1, 'days')};


  }
  rangeClicked(range) {

    this.from=range.startDate.format('YYYY-MM-DD');
    this.to=range.endDate.format('YYYY-MM-DD');

    // console.log('[rangeClicked] range is : ', range);
  }
  datesUpdated(range) {
    this.from=range.startDate.format('YYYY-MM-DD');
    this.to=range.endDate.format('YYYY-MM-DD');



    // console.log('[datesUpdated] range is : ', range);
  }

  ngOnInit() {

    this.dtOptions = {
      //responsive: true,
      dom: 'Bfrtip',
      lengthMenu:[100, 150, 200],
      button: {
        tag: 'button',
        className: 'btn btn-info'
      },
      // buttons: [
      //   'copy', 'csv', 'excel', 'pdf', 'print', 'colvis',
      //   ]
      buttons: [

         { extend: 'excel', className: 'btn btn-success',  init: function( api, node, config) {
          $(node).removeClass('dt-button buttons-excel buttons-html5')
       } },
       { extend: 'copy', className: 'btn btn-success',  init: function( api, node, config) {
        $(node).removeClass('dt-button buttons-excel buttons-html5')
     } },
     { extend: 'csv', className: 'btn btn-success',  init: function( api, node, config) {
      $(node).removeClass('dt-button buttons-excel buttons-html5')
   } },
   { extend: 'colvis', className: 'btn btn-success',  init: function( api, node, config) {
    $(node).removeClass('dt-button buttons-excel buttons-html5')
 } },
 { extend: 'pdf', className: 'btn btn-success',  init: function( api, node, config) {
  $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'print', className: 'btn btn-success',  init: function( api, node, config) {
  $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
      ]
    };

  }


  product_profitability(e){
    e.preventDefault();

    this.service.get_product_analysis(this.from, this.to,this.user).subscribe((data)=>{

      var info=JSON.parse(JSON.stringify(data))
      this.data = info;
      this.orders=info.orders;
      this.returns=info.returns;
      this.total_orders=info.total_orders;
      this.return_percent = info.return_percent;
      this.order_sum = info.order_sum;
      this.ret_sum = info.ret_sum;
      this.expected_rec_amt = info.expected_rec_amt;
      this.shipped_orders = info.shipped_orders;
      this.cancelled_orders = info.can_orders;


// console.log(info);
    });

  }

}
