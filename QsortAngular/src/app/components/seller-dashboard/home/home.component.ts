import { Component, OnInit } from '@angular/core';

import {Seller} from '../../../Models/seller';
import {Months} from '../../../Models/chart_months';
import {Amounts} from '../../../Models/chart_amount';
import { QsortService} from '../../../service/qsort.service';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  chartOptions = {
    responsive: true,
    scaleShowVerticalLines: false,
    scaleShowValues: true,
    scaleValuePaddingX: 10,
    scaleValuePaddingY: 10,
    animation: {
      onComplete: function () {
          var chartInstance = this.chart,
              ctx = chartInstance.ctx;

          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (line, index) {
                  var data = dataset.data[index];
                  ctx.fillText(data, line._model.x, line._model.y - 5);
              });
          });
      }
  }
  };

  chartLabels = [];
  chartdatastr =[];
  chartData = [
    { data: [], label: 'Sales' },
    { data: [], label: 'Returns' },

  ];




  onChartClick(event) {
    console.log(event);
  }

  Seller: Seller[];
  Months: Months[];
  Amounts:Amounts[];

  constructor(private qsortservice : QsortService) { }

  ngOnInit() {

    this.get_chart_sales();



  }


  get_chart_sales(){

    this.qsortservice.get_chart_sales().subscribe((all)=>{
      var info=JSON.parse(JSON.stringify(all))

      this.Months=info.months_data;
      this.Amounts=info.amount_data;

      var months_data = JSON.stringify(info.months_data);
      var months_data_parse = JSON.parse(months_data);

      var amounts_data = JSON.stringify(info.amount_data);
      var amounts_data_parse = JSON.parse(amounts_data);

      var ret_month_data = JSON.stringify(info.ret_month_data);
      var months_data_ret_parse = JSON.parse(ret_month_data);

      var ret_amount_data = JSON.stringify(info.ret_amount_data);
      var amounts_data_ret_parse = JSON.parse(ret_amount_data);


      var chartjsLabels = [];
      for (var i = 0; i < months_data_parse.length; i++) {
         chartjsLabels.push(months_data_parse[i].month_name);
      }
    //   for (var i = 0; i < months_data_ret_parse.length; i++) {
    //     chartjsLabels.push(months_data_ret_parse[i].month_name);
    //  }


      var chartdataLabels = [];
      for (var i = 0; i < amounts_data_parse.length; i++) {
        chartdataLabels.push(amounts_data_parse[i].shp_amt);
      }
      var chartreturnLabels = [];
      for (var i = 0; i < amounts_data_ret_parse.length; i++) {
        chartreturnLabels.push(amounts_data_ret_parse[i].shp_amt);
      }


this.chartLabels=chartjsLabels;


this.chartData[0].data = chartdataLabels;
this.chartData[1].data = chartreturnLabels;

console.log(info);
    })

  }

  
doughnutChartLabels: Label[] = ['Success Orders', 'Returened Orders'];
  doughnutChartData: MultiDataSet = [
    [65, 35]
  ];
  doughnutChartType: ChartType = 'doughnut';

}
