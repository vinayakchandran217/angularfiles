import { Component, OnInit } from '@angular/core';
import { Inventory } from '../../../Models/Inventory';
import { QsortService} from '../../../service/qsort.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  // dtOptions: DataTables.Settings = {scrollX: true,
  //   scrollY: '800',  };
  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  user = this.currentUser.user;

  dtOptions: any = {};
inventory: Inventory[];

// dataTable: any;
  constructor(private service : QsortService) { }

  ngOnInit() {

this.dtOptions = {
  responsive: false,
  columnDefs: [
            { width: 20, targets: 0 }
        ],
        fixedColumns: true,
  "order": [[5, 'desc']],
  dom: 'Bfrtip',
  buttons: [

    { extend: 'excel', className: 'btn btn-success',  init: function( api, node, config) {
     $(node).removeClass('dt-button buttons-excel buttons-html5')
  } },
  { extend: 'copy', className: 'btn btn-success',  init: function( api, node, config) {
   $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'csv', className: 'btn btn-success',  init: function( api, node, config) {
 $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'colvis', className: 'btn btn-success',  init: function( api, node, config) {
$(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'pdf', className: 'btn btn-success',  init: function( api, node, config) {
$(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'print', className: 'btn btn-success',  init: function( api, node, config) {
$(node).removeClass('dt-button buttons-excel buttons-html5')
} },
 ]
};
    this.get_inventory_from_db();
  }

get_inventory_from_db(){

  this.service.get_seller_inventory().subscribe((all)=>{
    this.inventory=all;


  })

}




GetInventory_comp(){
this.service.GetInventory().subscribe((all)=>{
  this.inventory=all;

  // const table: any = $('table');
  // this.dataTable = table.DataTable();
  location.reload();
})

}

update_cost(sku,cp){

  this.service.update_cost(sku,cp,this.user).subscribe((data)=>{

    location.reload();
// this.get_inventory_from_db();


  });





}


}
