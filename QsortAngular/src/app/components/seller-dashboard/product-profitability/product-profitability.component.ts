import { Component, OnInit } from '@angular/core';
import {Orders} from '../../../Models/Orders';

import { QsortService } from '../../../service/qsort.service';
import * as moment from 'moment';
import { Daterange } from 'src/app/Models/daterange';
@Component({
  selector: 'app-product-profitability',
  templateUrl: './product-profitability.component.html',
  styleUrls: ['./product-profitability.component.css']
})
export class ProductProfitabilityComponent implements OnInit {
  dtOptions: any = {};
  from:string;
  to:string;




data:Orders[];
  orders: Orders[];
  daterange:Daterange;



//   selected = {startDate: moment(), endDate: moment().add('3', 'days'),

// };

selected: any;

alwaysShowCalendars: boolean;
showRangeLabelOnInput: boolean;
keepCalendarOpeningWithRange: boolean;
maxDate: moment.Moment;
minDate: moment.Moment;
// invalidDates: moment.Moment[] = [moment().add(2, 'days'), moment().add(3, 'days'), moment().add(5, 'days')];
invalidDates: moment.Moment[] = [];
ranges: any = {
  // Today: [moment(), moment()],
  // Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  'This Month': [moment().startOf('month'), moment().endOf('month')],
  'Last Month': [
    moment()
      .subtract(1, 'month')
      .startOf('month'),
    moment()
      .subtract(1, 'month')
      .endOf('month')
  ]
};
isInvalidDate = (m: moment.Moment) =>  {
  return this.invalidDates.some(d => d.isSame(m, 'day') )
}





  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  user = this.currentUser.user;
  constructor(private service: QsortService) {

    this.maxDate = moment().add(2,  'weeks');
    this.minDate = moment().subtract(3, 'days');
    this.alwaysShowCalendars = true;
    this.keepCalendarOpeningWithRange = true;
    this.showRangeLabelOnInput = true;
    this.selected = {startDate: moment().subtract(1, 'days'), endDate: moment().subtract(1, 'days')};

    // this.r1 = this.selected.startDate;
    // this.from = new Date(this.r1);
    // this.s = this.from.toLocaleDateString();
  }

  rangeClicked(range) {

    this.from=range.startDate.format('YYYY-MM-DD');
    this.to=range.endDate.format('YYYY-MM-DD');

    // console.log('[rangeClicked] range is : ', range);
  }
  datesUpdated(range) {
    this.from=range.startDate.format('YYYY-MM-DD');
    this.to=range.endDate.format('YYYY-MM-DD');



    // console.log('[datesUpdated] range is : ', range);
  }

  ngOnInit() {

    this.dtOptions = {
      responsive: false,
      dom: 'Bfrtip',
      lengthMenu:[100, 150, 200],
      buttons: [

        { extend: 'excel', className: 'btn btn-success',  init: function( api, node, config) {
         $(node).removeClass('dt-button buttons-excel buttons-html5')
      } },
      { extend: 'copy', className: 'btn btn-success',  init: function( api, node, config) {
       $(node).removeClass('dt-button buttons-excel buttons-html5')
    } },
    { extend: 'csv', className: 'btn btn-success',  init: function( api, node, config) {
     $(node).removeClass('dt-button buttons-excel buttons-html5')
  } },
  { extend: 'colvis', className: 'btn btn-success',  init: function( api, node, config) {
   $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'pdf', className: 'btn btn-success',  init: function( api, node, config) {
 $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
{ extend: 'print', className: 'btn btn-success',  init: function( api, node, config) {
 $(node).removeClass('dt-button buttons-excel buttons-html5')
} },
     ]
    };


  }


  product_profitability(e){
    e.preventDefault();




    this.service.get_product_profitability(this.from,this.to,this.user).subscribe((data)=>{

      var info=JSON.parse(JSON.stringify(data))
      this.orders=info.ship_data;

console.log(info);
    });

  }

}







