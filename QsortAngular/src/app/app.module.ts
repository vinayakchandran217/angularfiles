import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';

import { QsortService } from './service/qsort.service';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AuthguardService } from './auth-guard';
import {AuthService} from './auth.service';


import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { DataTablesModule } from 'angular-datatables';

import {LoaderService} from './service/loader.service';
import {LoaderInterceptor} from './interceptors/loader.interceptor';


import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

import { ChartsModule } from 'ng2-charts';

import { SellerDashboardComponent } from './components/seller-dashboard/seller-dashboard.component';
import { HomeComponent } from './components/seller-dashboard/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { SiteHeaderComponent } from './components/_layout/site-header/site-header.component';
import { SiteFooterComponent } from './components/_layout/site-footer/site-footer.component';
import { SiteLayoutComponent } from './components/_layout/site-layout/site-layout.component';
import { SiteNavComponent } from './components/_layout/site-nav/site-nav.component';
import { SiteNav2Component } from './components/_layout/site-nav2/site-nav2.component';
import { FooterScriptsComponent } from './components/_layout/footer-scripts/footer-scripts.component';
 import {OrdersComponent} from './components/seller-dashboard/orders/orders.component';
 import { InventoryComponent } from './components/seller-dashboard/inventory/inventory.component';
import { LoaderComponent } from './components/loader/loader.component';
import { PaymentDataComponent } from './components/seller-dashboard/payment-data/payment-data.component';
import { ProductProfitabilityComponent } from './components/seller-dashboard/product-profitability/product-profitability.component';
import { OrderAnalysisComponent } from './components/seller-dashboard/order-analysis/order-analysis.component';
import {MatButtonModule,MatCheckboxModule,MatInputModule,MatCardModule,MatMenuModule, MatIconModule} from '@angular/material';
import { MyAccountComponent } from './components/my-account/my-account.component';










@NgModule({
  declarations: [
    AppComponent,
    SellerDashboardComponent,
    HomeComponent,
    LoginComponent,
    SiteHeaderComponent,
    SiteFooterComponent,
    SiteLayoutComponent,
    SiteNavComponent,
    SiteNav2Component,
    FooterScriptsComponent,
    OrdersComponent,
    InventoryComponent,
    LoaderComponent,
    PaymentDataComponent,
    ProductProfitabilityComponent,
    OrderAnalysisComponent,
    MyAccountComponent,




  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatTabsModule,
    BrowserAnimationsModule,
    DataTablesModule,
    NgxDaterangepickerMd.forRoot({
      separator: ' - ',
      applyLabel: 'Okay',
  }),
  ChartsModule,
  MatButtonModule, MatCheckboxModule,MatInputModule,MatCardModule,MatMenuModule,MatIconModule


  ],
  providers: [AuthguardService,
    AuthService,
    QsortService,
    LoaderService,{ provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true,},
    {provide: LocationStrategy, useClass: PathLocationStrategy}

  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
